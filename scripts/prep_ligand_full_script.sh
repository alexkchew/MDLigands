#!/bin/bash

# prep_ligand_full_script.sh
# The purpose of this script is to take a new ligand itp, prm, etc. file and run the following algorithm:
#   - energy minimization of the ligands
#   - fragment builder to create fragment
#   - create a ligand file for that particular fragment
#   - update the ligand list (if not already there!)

# Created on: 04/18/2018
# Written by: Alex K. Chew (alexkchew@gmail.com)

## USAGE EXAMPLE: 
#    bash prep_ligand_full_script.sh butanethiol BUT charmm36-jul2017.ff

## ASSUMPTIONS:
#   We assume you are using the CHARMM36 force field and generated the itp, pdb, and prm file from the CGENFF software
#   We also assume that you want the entire molecule as a fragment. This is highly useful in terms of ensuring that your force field is parameterized correctly.
## NOTE: We assume that you have downloaded your itp, prm, and _ini.pdb file within the Main_dir > raw_files. Afterwards, this script should take care of everything else.

## VARIABLES:
#   $1: input molecule name, e.g. butanethiol
#   $2: input residue name, e.g. BUT
#   $3: force field name, e.g. charmm36-jul2017.ff

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="global_funcs.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/${global_file_name}"

#######################
### INPUT VARIABLES ###
#######################

### INPUT VARIABLES
input_molecule_name="$1" # Name of the molecule you care about
input_molecule_resname="$2" # Residue name of the molecule
## DEFINING FORCE FIELD
forcefield="${3:-charmm36-jul2017.ff}"

## DEFINING BASH SCRIPTS
bs_prep_ligand_em="${main_dir}/prep_ligand_energy_min.sh"

## DEFINING PYTHON SRIPTS
py_prep_ligand="${MDBUILDER}/ligand/combine_head_group.py"

######################
### DEFINING PATHS ###
######################
### MISCELLANEOUS DIRECTORIES
prep_file="prep_files"
em_setup_files="em_setup_files"

### DEFINING INPUT GRO AND ITP FILE
input_gro_file="${input_molecule_name}_em.gro"
input_itp_file="${input_molecule_name}.itp"
input_prm_file="${input_molecule_name}.prm"

## DEFINING MOLECULE PATH
path_forcefield_molecule="${MDLIG_FINAL_LIGS}/${forcefield}"
path_molecule="${path_forcefield_molecule}/${input_molecule_name}"
## DEFINING PREP FILE
path_molecule_prep="${path_molecule}/${prep_file}"
## PATH TO SETUP FILE
path_em_setup_files="${path_molecule}/${em_setup_files}"

## DEFINING FILE PATH
path_molecule_em_itp="${path_molecule_prep}/${input_itp_file}" # itp file
path_molecule_em_gro="${path_em_setup_files}/${input_gro_file}"
path_molecule_em_prm="${path_molecule_prep}/${input_prm_file}"

########################
### OUTPUT VARIABLES ###
########################

## OUTPUT PATH
path_output="${path_molecule}"

## OUTPUT FILES
path_output_gro="${path_output}/${input_molecule_name}.gro"
path_output_prm="${path_output}/${input_molecule_name}.prm"
path_output_itp="${path_output}/${input_molecule_name}.itp"

## OUTPUT LIG NAMES
path_output_lig_names="${path_forcefield_molecule}/${LIG_NAMES_TXT}"

###################
### MAIN SCRIPT ###
###################

### RUNNING ENERGY MINIMIZATION BASH SCRIPT
bash "${bs_prep_ligand_em}" "${input_molecule_name}" "${input_molecule_resname}" "${forcefield}" 

### RUNNING PYTHON CODE
python "${py_prep_ligand}" --igro "${path_molecule_em_gro}" \
						   --iitp "${path_molecule_em_itp}" \
						   --ogro "${path_output_gro}" \
						   --oitp "${path_output_itp}"

## EDITING GRO FILE PBC
gmx editconf -f "${path_output_gro}" -o "${path_output_gro}" -d 0

## CLEANING ANY DUPLICATES
rm ${path_output}/\#*

## COPYING PARAMETER FILE
echo "*** COPYING PARAMETER FILE ***"
cp -rv "${path_molecule_em_prm}" "${path_output_prm}"

## CHECKING IF COMPLETION
if [ ! -e "${path_output_gro}" ] || [ ! -e "${path_output_itp}" ] || [ ! -e "${path_output_prm}" ]; then
    echo "ERROR! We are missing either gro, itp, prm file."
    echo "Please check ${path_output}"
    echo "You may be missing some inputs!"
    echo "Stopping here to prevent subsequent errors!"
    exit 1
fi

### RUNNING REFRESHING SYSTEM FOR LIGAND NAMES
bash "${PREP_LIG_REFRESH_FILE}" "${path_forcefield_molecule}" "${path_output_lig_names}"

## PRINTING SUMMARY
echo -e "\n********** SUMMARY **********"
echo "CREATED LIGANDS FOR: ${input_molecule_name}"
echo "RESIDUE NAME: ${input_molecule_resname}"
echo "FORCE FIELD: ${forcefield}"
echo "FULL PATH TO LIGAND: ${path_output}"
echo "Good luck in the simulations!"
