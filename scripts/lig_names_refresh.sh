#!/bin/bash

# lig_names_refresh.sh
# The purpose of this script is to update any ligands that may be missing from lig_names.txt

###

## USAGE EXAMPLE: 

# ALGORITHM:
#   - Load lig_names.txt file
#   - Loop through the available ligands
#   - Find if the ligand is not there --- if so, extract the details

# Created on: 04/19/2018
# Written by: Alex K. Chew (alexkchew@gmail.com)

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="global_funcs.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/${global_file_name}"

## STORING PREVIOUS IFS (separator)
OLDIFS="$IFS"

## PRINTING 
echo -e "\n----- lig_names_refresh.sh ----- \n"

## STORING IFS
OLDIFS="$IFS"

#######################
### INPUT VARIABLES ###
#######################

## DEFINING LIGAND DIRECTORY
path_ligand_dir="$1" # "final_ligands"

## DEFINING TEXT FILE
path_ligand_txt="$2" # "lig_names.txt"


## CREATING LIGAND TEXT FILE IF IT DOES NOT EXIST
touch "${path_ligand_txt}"

#################################
### ANALYZING THE LIGAND FILE ###
#################################

## READING THE LIGAND NAME FILE
## READING THE FILE WITHOUT COMMENTS AND SPACES
read_file="$(grep -v -e '^$' "${path_ligand_txt}" | grep -v -e '^;')"

### READING FILES AS ARRAYS
readarray -t l_lig_names <<< "$(echo "${read_file}" | awk -F', ' '{print $1}')"
readarray -t l_res_names <<< "$(echo "${read_file}" | awk -F', ' '{print $2}')"

##################################
### ANALYZING LIGAND DIRECTORY ###
##################################

### GETTING LIGAND NAMES (WITHOUT PARENT DIRECTORY)
readarray -t l_dir_lig_names <<< "$(ls ${path_ligand_dir}/*/ -d | xargs -n 1 basename)"

## CREATING ARRAY TO STORE WHICH LIGANDS WERE ADDED
store_ligand_names=()

### LOOPING THROUGH EACH LIGAND
for each_ligand in ${l_dir_lig_names[@]}; do
    ## GETTING LIGAND WITHOUT THE .LIG FLAGA
    ligand_nolig=${each_ligand%.lig}
    ## PRINTING
    # echo "Checking ligand: ${ligand_nolig}"
    ## CHECKING IF LIGAND IS WITHIN LIGAND TEXT
    if [[ " ${l_lig_names[@]} " =~ " ${each_ligand} " ]]; then
        ## LIGAND IS IN THIS LIST
        echo "${each_ligand} is in the list, not doing anything further!"
    else
        echo "${each_ligand} is not in the list!"
        ## READING LIGAND FILE
        itp_resname=$(extract_itp_resname "${path_ligand_dir}/${each_ligand}/${each_ligand}.itp")
        echo "Molecule name: ${each_ligand}, Residue name: ${itp_resname}"
        ## ADDING TO LIGAND FILE
        echo "${each_ligand}, ${itp_resname}" >> "${path_ligand_txt}"
        ## ADDING TO STORAGE
        store_ligand_names+="${each_ligand} "
    fi
done

echo -e "\n~~~~~ SUMMARY ~~~~~"
echo "Added the following ligands to ${path_ligand_txt}: ${store_ligand_names[@]}"
