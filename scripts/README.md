# scripts

This folder contains all important scripts. Below describes what is the role of each script.

# Description

* `cgenff_charmm2gmx_py2.py`: CGenFF 4.0 for python 2
* `cgenff_charmm2gmx_py3.py`: CGenFF 4.0 for python 3
* `global_funcs.sh`: global functions for this module
* `global_vars.sh`: global variables for this module
* `lig_names_refresh.sh`: script to refresh ligand name text file
* `prep_ligand_energy_min.sh`: script to energy minimize the ligand
* `prep_ligand_full_script.sh`: also energy minimizes, but also refreshes the ligand names
* `prep_ligands_charmm36.sh`: script that uses cgenff scripts to convert mol2 and str files to GROMACS Files