#!/bin/bash
# global_vars.sh
# This contains all global variables for this project.

####################################################
######### ADJUSTABLE VARIABLES BASED ON USER #######
####################################################

## DEFINING MDBUILDER
MDBUILDER="${HOME}/bin/pythonfiles/modules/MDBuilder"
if [ $USER == "bdallin" ]; then
    MDBUILDER="${HOME}/bin/python_modules/MDBuilder"
fi
    

##################################################
###### VARIABLES THAT DO NOT NEED ADJUSTING ######
##################################################

## DEFINING SCRIPT DIRECTORY
MDLIG_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

## DEFINING IMPORTANT SCRIPTS WITHIN SCRIPT DIRECTOR
PREP_LIG_REFRESH_FILE="${MDLIG_SCRIPT_DIR}/lig_names_refresh.sh"
LIG_NAMES_TXT="lig_names.txt" # Text file to store all ligand names

## DEFINING MAIN DIRECTORY
MDLIG_MAIN_DIR=$(dirname "${MDLIG_SCRIPT_DIR}")

## DEFINING FORCE FIELD DIRECTORY
MDLIG_FF_DIR="${MDLIG_MAIN_DIR}/forcefield"

## DEFINING RAW FILES DIR
MDLIG_RAW_DIR="${MDLIG_MAIN_DIR}/raw_files"

## DEFINING FINAL LIGANDS
MDLIG_FINAL_LIGS="${MDLIG_MAIN_DIR}/final_ligands"

## DEFINING INPUT FILES
MDLIG_INPUT_FILES="${MDLIG_MAIN_DIR}/input_files"
MDLIG_INPUT_FILES_MDP="${MDLIG_INPUT_FILES}/mdp"
MDLIG_INPUT_FILES_TOP="${MDLIG_INPUT_FILES}/topology"

## DEFINING SOLVENT FILES
MDLIG_SOLVENTS="${MDLIG_MAIN_DIR}/solvents"
MDLIG_SOLVENTS_RAW="${MDLIG_SOLVENTS}/raw_files"
MDLIG_SOLVENTS_FINAL="${MDLIG_SOLVENTS}/final_structures"

## DEFINING SURFACTANT FILES
MDLIG_SURFACTANTS="${MDLIG_MAIN_DIR}/surfactants"
MDLIG_SURFACTANTS_RAW="${MDLIG_SURFACTANTS}/raw_files"
MDLIG_SURFACTANTS_FINAL="${MDLIG_SURFACTANTS}/final_structures"

## DEFINING PEPTIDE FILES
MDLIG_PEPTIDES="${MDLIG_MAIN_DIR}/peptides"
MDLIG_PEPTIDES_RAW="${MDLIG_PEPTIDES}/raw_files"
MDLIG_PEPTIDES_FINAL="${MDLIG_PEPTIDES}/final_structures"
