#!/bin/bash
# 
# prep_ligands_charmm36.sh
# 
# The purpose of this code is to prepare ligands by doing the following:
#   - copying str, mol2, and renaming if necessary
#   - run cgenff to create forcefield files
#   - rename all outputs of cgenff accordingly
# 
# Written by: Alex K. Chew (alexkchew@gmail.com)
# Created on: 03/08/2019
# USAGE EXAMPLE:
#   bash prep_ligands_charmm36.sh butanethiol BUT charmm36-jul2017.ff
#
## VARIABLES:
#   $1: input molecule name, e.g. butanethiol
#   $2: input residue name, e.g. BUT
#   $3: force field name, e.g. charmm36-jul2017.ff
#   $4: cgenff script, default: cgenff_charmm2gmx_py2.py
#   $5: raw path to *.mol2 and *.str file 
#   $6: path to final directory to store all outputs
#   $7: Python key word
### UPDATES ###
# 2019-05-30: Updated script to use raw and final directories as input/output

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="global_funcs.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/${global_file_name}"

################################
### DEFINING INPUT VARIABLES ###
################################

## DEFINING VARIABLES
input_molecule_name="$1"
input_residue_name="$2"
## DEFINING FORCE FIELD
forcefield="${3:-charmm36-mar2019.ff}"
## DEFINING PYTHON SCRIPT
python_script="${4:-cgenff_charmm2gmx_py2.py}"

## DEFINING RAW DIRECTORY
path_raw_dir="${5:-${MDLIG_RAW_DIR}}"
path_final_dir="${6:-${MDLIG_FINAL_LIGS}}"

## DEFINING PYTHON
current_python=${7-python2.7}

######################
### DEFINING PATHS ###
######################
### MISCELLANEOUS DIRECTORIES
prep_file="prep_files"

# Note: This uses global variables to define force field paths
## FORCE FIELD PATH
path_forcefield="${MDLIG_FF_DIR}/${forcefield}"
## MOLECULE PATH
path_molecule_mol2="${path_raw_dir}/${input_molecule_name}.mol2"
path_molecule_str="${path_raw_dir}/${input_molecule_name}.str"
## PYTHON PATH
path_python="${MDLIG_SCRIPT_DIR}/${python_script}"
## DEFINING OUTPUT DIRECTORY PATH
path_output="${path_final_dir}/${forcefield}/${input_molecule_name}"
## DEFINING PATH TO SETUP
path_output_setup="${path_output}/${prep_file}"

### CHECKING IF ALL PATHS EXISTS (STOP OTHERWISE)
echo -e "******** CHECKING PATHS ********"
stop_if_does_not_exist "${path_forcefield}"
stop_if_does_not_exist "${path_molecule_mol2}"
stop_if_does_not_exist "${path_molecule_str}"
stop_if_does_not_exist "${path_python}"
echo "--------------------------------------"

## CHECKING IF RESIDUE NAME IS DEFINED
if [ -z "${input_residue_name}" ]; then
    echo "Error! Input residue name not specified! Check your inputs!"
    exit 1
fi

#####################
### MAIN COMMANDS ###
#####################

## CREATING DIRECTORY
create_dir "${path_output}" -f 

## MOVING TO DIRECTORY
cd "${path_output}"

## PRINTING
echo "*** RUNNING PREPARATION CHARMM36 ***"
echo "MOLECULE FULL NAME: ${input_molecule_name}"
echo "MOLECULE RESIDUE NAME: ${input_residue_name}"

#########################################
### FIXING RESIDUE NAMES FOR STR FILE ###
#########################################
echo "*******"
echo "FIXING RESIDUE NAMES OF MOL2 AND STR TO: ${input_residue_name}"

## FINDING RESIDUE NAME IN STR FILE
current_residue_name="$(grep "\bRESI\b" ${path_molecule_str} | awk '{print $2}' | sed -e 's/[]\/$*.^[]/\\&/g')"
## GETTING 
if [ "${current_residue_name}" != "${input_residue_name}" ]; then
    echo "STR FILE: CORRECTING RESIDUE NAME ${current_residue_name} to ${input_residue_name}" ## TODO: Check out what is going on here with sed -- str may not be sufficient

    ## GETTING LINE NUMBER FOR RESI
    line_number_resi=$(grep -nE "\bRESI\b" ${path_molecule_str} | sed 's/\([0-9]*\).*/\1/')

    ## EDITING THAT SPECIFIC LINE NUMBER
    sed -i "${line_number_resi}s/${current_residue_name}/${input_residue_name}/1" ${path_molecule_str}

    ## DEPRECIATED SINCE IT MIGHT AFFECT ATOMIC COORDINATES, E.G. NEW RESIDUE NAME IS 000
    # sed -i "s/${current_residue_name}/${input_residue_name}/g" ${path_molecule_str}
fi

##########################################
### FIXING RESIDUE NAMES FOR MOL2 FILE ###
##########################################
current_residue_name="$(mol2_find_residue_name ${path_molecule_mol2} | sed -e 's/[]\/$*.^[]/\\&/g')"
if [ "${current_residue_name}" != "${input_residue_name}" ]; then
    echo "MOL2 FILE: CORRECTING RESIDUE NAME ${current_residue_name} to ${input_residue_name}"
    ## GETTING LINE NUMBER FOR MOLECULE IN MOL2
    line_num_mol2_molecule=$(grep -nE "@<TRIPOS>MOLECULE" "${path_molecule_mol2}"| sed 's/\([0-9]*\).*/\1/')
    ## EDITING THAT SPECIFIC LINE NUMBER
    line_num_mol2_molecule_plus1=$((${line_num_mol2_molecule} + 1))
    ## EDITING SPECIFIC LINE NUMBER, AND ONLY ONCE
    sed -i "${line_num_mol2_molecule_plus1}s/${current_residue_name}/${input_residue_name}/1" ${path_molecule_mol2}

fi

######################
### RUNNING CGENFF ###
######################

## RUNNING CGENFF
echo "RUNNING COMMAND: python ${path_python} "${input_residue_name}" "${path_molecule_mol2}" "${path_molecule_str}" "${path_forcefield}""
${current_python} ${path_python} "${input_residue_name}" "${path_molecule_mol2}" "${path_molecule_str}" "${path_forcefield}"

###############################
### CHANGING NAMES OF FILES ###
###############################
input_residue_name_lowercase="$(echo ${input_residue_name} |tr '[:upper:]' '[:lower:]')"

## CHECKING IF FILE EXISTS
if [ ! -e "${input_residue_name_lowercase}.itp" ] || [ ! -e "${input_residue_name_lowercase}.prm" ] || [ ! -e "${input_residue_name_lowercase}_ini.pdb" ];
then
    echo "Error! One or more of the following files do not exist:"
    echo "ITP file: ${input_residue_name_lowercase}.itp"
    echo "PDB file: ${input_residue_name_lowercase}_ini.pdb"
    echo "PRM file: ${input_residue_name_lowercase}.prm"
    echo "There may be something wrong with running the python script! Please check for error output, stopping here!"
else
    ## CREATING DIRECTORY AND COPYING ALL THE FILES
    echo "CREATING DIRECTORY: ${path_output_setup}"
    mkdir -p ${path_output_setup}

    ## RENAMING THE OUTPUTS ACCORDINGLY
    mv ${input_residue_name_lowercase}.top ${path_output_setup}/${input_molecule_name}.top
    mv ${input_residue_name_lowercase}.itp ${path_output_setup}/${input_molecule_name}.itp
    mv ${input_residue_name_lowercase}.prm ${path_output_setup}/${input_molecule_name}.prm
    mv ${input_residue_name_lowercase}_ini.pdb ${path_output_setup}/${input_molecule_name}.pdb
    echo "MOVED ALL ITP, PRM, AND TOP FILES TO: ${path_output_setup}"
fi


