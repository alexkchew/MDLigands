#!/bin/bash

# prep_energy_min.sh
# This script runs energy minimizations on specific ligands to quickly assess if the force field parameters are correct.
# This code does the following:
#   - copies over topology file, renames everything
#   - runs a quick energy minimization on the current ligand
#
# Originally written by: Alex K. Chew (alexkchew@gmail.com)
# Created on: 02/01/2018
#
# ** UPDATES **
#   20180418: Updating script to work with full_script ligand preparation
#   20190508: Re-adapting the code for MDLigands
#   20190530: Updated code to final structures
#
## USAGE EXAMPLE:
#   bash prep_ligand_energy_min.sh butanethiol BUT charmm36-jul2017.ff
#
## VARIABLES:
#   $1: input molecule name, e.g. butanethiol
#   $2: input residue name, e.g. BUT
#   $3: force field name, e.g. charmm36-jul2017.ff
#   $4: path to final structure

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="global_funcs.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/${global_file_name}"

################################
### DEFINING INPUT VARIABLES ###
################################

### INPUT VARIABLES
input_molecule_name="$1" # Name of the molecule you care about
input_molecule_resname="$2" # Residue name of the molecule
## DEFINING FORCE FIELD
forcefield="${3:-charmm36-jul2017.ff}"

## DEFINING INPUT FINAL STRUCTURE FOLDER
path_final_structure="${4:-${MDLIG_FINAL_LIGS}}"

######################
### DEFINING PATHS ###
######################
### MISCELLANEOUS DIRECTORIES
prep_file="prep_files"
em_setup_files="em_setup_files"

## DEFINING MOLECULE PATH
path_molecule="${path_final_structure}/${forcefield}/${input_molecule_name}"
## DEFINING PREP FILE
path_molecule_prep="${path_molecule}/${prep_file}"
## DEFINING FILE PATH
path_molecule_itp="${path_molecule_prep}/${input_molecule_name}.itp" # itp file
path_molecule_pdb="${path_molecule_prep}/${input_molecule_name}.pdb"
path_molecule_prm="${path_molecule_prep}/${input_molecule_name}.prm"
## PATH TO SETUP FILE
path_em_setup_files="${path_molecule}/${em_setup_files}"
## DEFINING PATH TO FORCE FIELD
path_forcefield="${MDLIG_FF_DIR}/${forcefield}"

### CHECKING IF ALL PATHS EXISTS (STOP OTHERWISE)
echo -e "******** CHECKING PATHS ********"
stop_if_does_not_exist "${path_molecule}"
stop_if_does_not_exist "${path_molecule_prep}"
stop_if_does_not_exist "${path_molecule_itp}"
stop_if_does_not_exist "${path_molecule_pdb}"
stop_if_does_not_exist "${path_molecule_prm}"
echo "--------------------------------------"

########################################
### DEFINING IMPORTANT DEFAULT FILES ###
########################################
### TOPOLOGY FILE
## NEED TO CHANGE "MOLECULE" WITHIN TOPOLOGY
input_topology_file_name="em.top" # default topology file
input_topology_file_path="${MDLIG_INPUT_FILES_TOP}/${input_topology_file_name}" 

### MDP FILE
input_mdp_file_name="em.mdp"
input_mdp_file_path="${MDLIG_INPUT_FILES_MDP}/${input_mdp_file_name}"

### DEFINING OUTPUT DIRECTORY
path_output="${path_em_setup_files}"

### DEFINING OUTPUT PARMAETERS
box_size="5" # Length of the box

### DEFINING OUTPUT FILES
output_gro_file_name="${input_molecule_name}.gro"

#####################
### MAIN COMMANDS ###
#####################

## CREATING DIRECTORY
create_dir "${path_output}" -f 

## MOVING TO DIRECTORY
cd "${path_output}"; echo "Going into: ${path_output}"

## COPYING TOPOLOGY FILE TO DIRECTORY
echo "Copying topology for ${input_molecule_name}"
cp -r "${input_topology_file_path}" "${path_output}"
## DEFINING TOPOLOGY FORCEFIELD WITH ITP
path_forcefield_with_itp="${path_forcefield}/forcefield.itp"
## EDITING TOPOLOGY FILE
sed -i "s#FORCEFIELD_PATH#${path_forcefield_with_itp}#g" "${input_topology_file_name}"
sed -i "s/RESNAME/${input_molecule_resname}/g" "${input_topology_file_name}"
sed -i "s#MOLECULE_PRM_PATH#${path_molecule_prm}#g" "${input_topology_file_name}"
sed -i "s#MOLECULE_ITP_PATH#${path_molecule_itp}#g" "${input_topology_file_name}"

###########################
### CENTER THE MOLECULE ###
###########################
echo -e "\n***** CENTERING MOLECULE *****"
echo "Converting $(basename ${path_molecule_pdb}) to ${output_gro_file_name}"
echo "Box size: ${box_size} nm"
gmx editconf -f "${path_molecule_pdb}" -o "${output_gro_file_name}" -box "${box_size}" "${box_size}" "${box_size}" >/dev/null 2>&1 # silencing

###########################
### ENERGY MINIMIZATION ###
###########################
### RUNNING ENERGY MINIMIZATION
echo -e "\n***** ENERGY MINIMIZATION *****"
echo "Energy minimizing GRO file: ${output_gro_file_name}"
gmx grompp -maxwarn 5 -f ${input_mdp_file_path} -c ${output_gro_file_name} -p ${input_topology_file_name} -o "${input_molecule_name}"_em.tpr
gmx mdrun -v -nt 1 -deffnm "${input_molecule_name}"_em

echo -e "\n***** PDB GENERATION *****"
echo "Creating pdb file from ${input_molecule_name}_em.gro to ${input_molecule_name}_em.pdb"

### CREATING A PDB FILE FOR THE SAKE OF VISUALIZATION
gmx editconf -f "${input_molecule_name}_em.gro" -o "${input_molecule_name}_em.pdb" >/dev/null 2>&1 # silencing

### CLEANING UP FILES
## REMOVING EDR FILES
if [ -e "${input_molecule_name}_em.edr" ]; then
    rm "${input_molecule_name}_em.edr"
fi

## REMOVING TRR FILES
if [ -e "${input_molecule_name}_em.trr" ]; then
    rm "${input_molecule_name}_em.trr"
fi

### PRINTING SUMMARY
echo "********** DONE *********"
echo "Finalized GRO after minimization: ${input_molecule_name}_em.gro"
echo "Available in directory: ${path_output}"
echo "Now, you can use the GRO file for subsequent calculations"
echo "Good luck!"
