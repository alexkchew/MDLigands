#!/bin/bash
# global_funcs.sh
# This script contains all global functions within this project. 
# AVAILABLE FUNCTIONS:
#   stop_if_does_not_exist: stops if file does not exist
#   mol2_find_residue_name: finds residue name from .mol2 files
#   create_dir: creates directory
#   check_file_exist: checks if file exists
#   prep_charmm36_molecule: prepares charmm36 molecules
#   extract_itp_resname: extracts itp resname


## SOURCING THE GLOBAL VARIABLES (assumed to be within same folder)
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

## DEFINING GLOBAL VARS NAME
global_vars_file_name="global_vars.sh"

## SOURCING GLOBAL VARIABLES
source "${script_dir}/${global_vars_file_name}"


## FUNCTION TO CHECK IF FILE EXISTS AND STOP IF IT DOES NOT
# The purpose of this function is to check if a file exists. If it does not, then stop the script!
#   $1: full path to file
# USAGE: stop_if_does_not_exist $FILE
function stop_if_does_not_exist () {
    ## PRINTING
    echo "Checking if file exists: $1 ..."
    if [ -e "$1" ]
    then
        echo "--> $1 exists, continuing!"
    else
        echo "XXX $1 does not exist!"
        echo "Check for errors! You may have a non-existant, vital file!"
        echo "Pausing here for 5 seconds so you can see this error! ..."
        sleep 5
        exit 1
    fi
}

### FUNCTION TO FIND THE RESIDUE NAME OF A MOL2 FILE
# The purpose of this function is to find the residue name of a mol2 file
# INPUTS:
#   $1: mol2 file
# OUTPUTS:
#   residue_name <-- residue name within mol2 file
## USAGE EXAMPLE: mol2_find_residue_name ROT001.mol2
function mol2_find_residue_name () {
    ## DEFINING INPUTS
    mol2_file_="$1"

    ## FINDING SPECIFIC MOLECULE LINE
    molecule_line_=$(grep -nr "@<TRIPOS>MOLECULE" "${mol2_file_}" | sed 's/\([0-9]*\).*/\1/')
    ## DEFINING LINE OF RESIDUE NAME
    residue_name_line_=$((${molecule_line_}+1))
    ## GETTING A SPECIFIC LINE
    residue_name_="$(sed -n ${residue_name_line_}'p' ${mol2_file_})"
    ## PRINTING
    echo "${residue_name_}"
}

## FUNCTION TO CHECK IF THE FILE EXISTS
# This function simply checks if a file exists and outputs true/false
# $1: Full path to file
# USAGE: check_file_exist $FILE
function check_file_exist () {
    if [ -e "$1" ]
    then
        echo "True"
    else
        echo "False"
    fi
}

### FUNCTION TO CHECK IF DIRECTORY EXISTS
# This function checks if the directory exists. If so, prompt removal
# $1: Directory you are interested in
# $2: -f <-- forced, create create directory without prompt
# USAGE: create_dir $DIRECTORY -f
function create_dir () {
    # DEFINING VARIABLES
    directory="$1"
    # CHECK IF DIRECTORY EXISTS
    dir_exists=$(check_file_exist ${directory})
    if [ "${dir_exists}" == "True" ]; then
        if [ "$2" != "-f" ]; then
            echo "${directory} already exists! Do you want to delete and recreate? (y/n)"
            read deletion_criteria
            while [ "${deletion_criteria}" != "y" ] && [ "${deletion_criteria}" != "n" ]; do
                echo "Error! Incorrect prompt! Deletion criteria can only be \"y\" or \"n\", try again:"
                read deletion_criteria
            done
        else # Forcing is on!
            deletion_criteria="y"
        fi
        
        if [ "${deletion_criteria}" == "y" ]; then
            echo "Deleting and recreating ${directory}, pausing 3 seconds..."
            sleep 3
            rm -rv "${directory}"
            mkdir -p "${directory}"
        elif [ "${deletion_criteria}" == "n" ]; then
            echo "Stopping here -- deletion prevented"
            echo "Check if you need these files. This error message is to prevent overwriting of data"
            exit
        fi
    else
        echo "Creating ${directory}"
        mkdir -p "${directory}"
    fi
}

### FUNCTION TO EXTRACT NAME OF RESIDUE GIVEN ITP FILE
# This function looks into your itp file and extracts the residue name from it
# $1: ITP file
# Usage: extract_itp_resname itp_file_name
function extract_itp_resname () {
    itp_file="${1%.itp}"
    # Defining itp file
    itp_file="${itp_file}.itp"

    # Finding residue name
    resname=$(sed '/^;/ d' "${itp_file}" | grep -E -A 1 '\[ moleculetype \]' | tail -n1 | awk '{print $1}') # Removes comments, finds molecule type, looks at second line, then prints first column
    
    # Printing to variable
    echo "${resname}"
}

### FUNCTION TO READ THE MOLECULES FOR A FORCE FIELD
function list_ligands_for_forcefield ()
# The purpose of this function is to list the ligands for a given force field
{   
    ## DEFINING FORCE FIELD
    forcefield="${1:-charmm36-jul2017.ff}"
    ## DEFINING TEXT FILE
    text_file="${MDLIG_FINAL_LIGS}/${forcefield}/${LIG_NAMES_TXT}"
    
    ## SEEING IF EXISTS
    if [ ! -e "${text_file}" ]; then
        echo "Error! ${text_file} does not exist!"
        echo "Perhaps double-check your force field: ${forcefield}"
        echo "Available force fields: "
        echo "-------"
        ls ${MDLIG_FF_DIR} -1
        echo "-------"
        echo "Stopping script ..."; sleep 3
        exit 1
    else
        echo "-----------------------"
        echo "Printing ligand names in ${text_file}"
        echo "-----------------------"
        cat "${text_file}"
    fi

}



