#!/bin/bash

# prep_single_ligand.sh
# This script uses all available script to generate a single ligand
# This will run through CGENFF, energy minimization, then transformation of head groups
# Originally written by: Alex K. Chew (alexkchew@gmail.com)
# Created on: 05/09/2019
## VARIABLES:
#   $1: input molecule name, e.g. butanethiol
#   $2: input residue name, e.g. BUT
#   $3: force field name, e.g. charmm36-jul2017.ff
#   $4: cgenff script, default: cgenff_charmm2gmx_py2.py
## USAGE:
#   bash prep_single_ligand.sh dodecanethiol DEC charmm36-jul2017.ff cgenff_charmm2gmx_py2.py

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING SCRIPTS DIRECTORY
scripts_dir="scripts"
## DEFINING GLOBAL VARS NAME
global_file_name="global_funcs.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/${scripts_dir}/${global_file_name}"

################################
### DEFINING INPUT VARIABLES ###
################################

## DEFINING VARIABLES
input_molecule_name="$1"
input_residue_name="$2"
## DEFINING FORCE FIELD
forcefield="${3:-charmm36-jul2017.ff}"
## DEFINING PYTHON SCRIPT
python_script="${4:-cgenff_charmm2gmx_py2.py}"

####################
### BASH SCRIPTS ###
####################
## CONVERSION USING CGENFF
prep_lig_to_cgenff="${MDLIG_SCRIPT_DIR}/prep_ligands_charmm36.sh"
## GENERATING FULL LIGAND
prep_em_lig="${MDLIG_SCRIPT_DIR}/prep_ligand_full_script.sh"

###################
### MAIN SCRIPT ###
###################

echo ""
echo "--> Running command: bash "${prep_lig_to_cgenff}" "${input_molecule_name}" "${input_residue_name}" "${forcefield}" "${python_script}""
## USING CGENFF FOR THE SPECIFIC LIGAND
bash "${prep_lig_to_cgenff}" "${input_molecule_name}" "${input_residue_name}" "${forcefield}" "${python_script}"

echo ""
echo "--> Running command: bash ${prep_em_lig} ${input_molecule_name} ${input_residue_name} ${forcefield}"
## GENERATING FULL LIGAND
bash "${prep_em_lig}" "${input_molecule_name}" "${input_residue_name}" "${forcefield}"
