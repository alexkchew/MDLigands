#!/bin/bash

# prep_multi_solvents.sh
# This script prepares multiple solvents. We will use the preparation single ligand code and expand on it.
# Originally written by: Alex K. Chew (alexkchew@gmail.com)
# Created on: 05/09/2091
## USAGE:
#   bash prep_multi_ligands.sh
# ACTIVATE PY36_MDD
# conda activate py36_mdd


#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING SCRIPTS DIRECTORY
scripts_dir="scripts"
## DEFINING GLOBAL VARS NAME
global_file_name="global_funcs.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/${scripts_dir}/${global_file_name}"

#############################
### DEFINING INPUT SCRIPT ###
#############################

## SINGLE LIGAND SCRIPT
prep_single_script="${MDLIG_MAIN_DIR}/prep_single_solvent.sh"

#############################
### PRE-DEFINED VARIABLES ###
#############################

## DEFINING FORCE FIELD
forcefield="charmm36-jul2020.ff"
# "charmm36-jul2017.ff"

## DEFINING CGENFF CODE
cgenff_script="cgenff_charmm2gmx_py2.py"
cgenff_script="cgenff_charmm2gmx_py3_nx2.py"

## DEFINING FOLDERS
## FOR SOLVENTS
path_raw_dir="${MDLIG_SOLVENTS_RAW}"
path_final_dir="${MDLIG_SOLVENTS_FINAL}"

# ## FOR SURFACTANTS
# path_raw_dir="${MDLIG_SURFACTANTS_RAW}"
# path_final_dir="${MDLIG_SURFACTANTS_FINAL}"

#######################
### CREATING ARRAYS ###
#######################
## DEFINING MOLECULE NAME
declare -a solvent_molecule_name=("octanol" "dmso")
# "methane"
# "tetramethylammonium"
# "methylammonium" "formate" "aceticacid"
# "formicacid" "formamide" "dimethylsulfide" "phenol" "toluene" "indole" "methanethiol"

## DEFINING RESIDUE NAME (MUST MATCH MOLECULE NAME!)
declare -a solvent_residue_name=("OT" "DMSO")
# "MTE"
# "TMA"
# "MTA" "FMT" "ACA"
# "FMA" "FMD" "DMS" "PHE" "TOL" "IND" "MTH"

###################
### MAIN SCRIPT ###
###################

## FINDING TOTAL MOLECULES
total_molecules=$((${#solvent_molecule_name[@]}-1))

## LOOPING THROUGH EACH MOLECULE NAME
for index in $(seq 0 ${total_molecules}); do
    ## DEFINING MOLECULE NAME AND RESIDUE NAME
    molecule_name="${solvent_molecule_name[index]}"
    molecule_resname="${solvent_residue_name[index]}"
    ## RUNNING PREPARATION CODE
    bash "${prep_single_script}" "${molecule_name}" "${molecule_resname}" "${forcefield}" "${cgenff_script}" "${path_raw_dir}" "${path_final_dir}"
done



