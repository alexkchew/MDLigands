#!/bin/bash

# prep_multi_database_lig.sh
# This script prepares multiple solvents. We will use the preparation single ligand code and expand on it.
# Originally written by: Alex K. Chew (alexkchew@gmail.com)
# Created on: 05/09/2020
## USAGE:
#   bash prep_multi_ligands.sh
# NOTES:
#	Check networkx version
# >>> import networkx
# print(networkx.__version__)
# ACTIVATE PY36_MDD
# conda activate py36_mdd


#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING SCRIPTS DIRECTORY
scripts_dir="scripts"
## DEFINING GLOBAL VARS NAME
global_file_name="global_funcs.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../${scripts_dir}/${global_file_name}"

#############################
### DEFINING INPUT SCRIPT ###
#############################

## SINGLE LIGAND SCRIPT
prep_single_script="${main_dir}/prep_multi_single_lig.sh"

## STRETCH CODE
prep_stretch_script="${main_dir}/prep_stretch_ligand.sh"

## DEFINING PYTHON
current_python="python"

#############################
### PRE-DEFINED VARIABLES ###
#############################

## DEFINING FORCE FIELD
forcefield="charmm36-jul2020.ff"
# "charmm36-jul2017.ff"

## DEFINING CGENFF CODE
cgenff_script="cgenff_charmm2gmx_py3_nx2.py"

## DEFINING CSV FILE
csv_database_file="logP_exp_data_LIGANDS.csv"

## DEFINING FOLDERS
## FOR SOLVENTS
path_raw_dir="${main_dir}/raw_files"
path_final_dir="${main_dir}/final_structures"

## DEFINING PATH TO CSV
path_csv="${main_dir}/${csv_database_file}"

## READING CSV FILE
# read -a molecule_names <<< $(tail -n +2 "${path_csv}" | awk -F',' '{ print $1 }')
# read -a molecule_resnames <<< $(tail -n +2 "${path_csv}" | awk -F',' '{ print $2 }')

declare -a molecule_names=(\
						"LIG7" \
						"LIG20" \
						"LIG57" \
						"LIG76" \
						"LIG81" \
						"LIG89" \
						)
declare -a molecule_resnames=(\
					    "L007" \
					    "L020" \
					    "L057" \
					    "L076" \
					    "L081" \
					    "L089" \
					    )

read -a molecule_names <<< $(tail -n +2 "${path_csv}" | awk -F',' '{ print $1 }')
read -a molecule_resnames <<< $(tail -n +2 "${path_csv}" | awk -F',' '{ print $2 }')

###################
### MAIN SCRIPT ###
###################

## FINDING TOTAL MOLECULES
total_molecules=$((${#molecule_names[@]}-1))

## LOOPING THROUGH EACH MOLECULE NAME
for index in $(seq 0 ${total_molecules}); do
    ## DEFINING MOLECULE NAME AND RESIDUE NAME
    molecule_name="${molecule_names[index]}"
    molecule_resname="${molecule_resnames[index]}"
    ## RUNNING PREPARATION CODE
    bash "${prep_single_script}" "${molecule_name}" "${molecule_resname}" "${forcefield}" "${cgenff_script}" "${path_raw_dir}" "${path_final_dir}" "${current_python}"

    ## RUNNING STRETCH CODE
    bash "${prep_stretch_script}" "${molecule_name}" "${forcefield}"

done



