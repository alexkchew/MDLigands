#!/bin/bash

# prep_stretch_ligand.sh
# The purpose of this script is to stretch the ligand accordingly and 
# run a quick simulation to elongate the ligand. 
# The idea would be to freeze one of the sulfurs, then 
# stretch the ligand accordingly. Then, output a gro file 
# that is for the stretched ligand. 
# 
# Note:
#	- We assume energy minimization was completed! 
#	- We will use energy minimized structures for topology

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING SCRIPTS DIRECTORY
scripts_dir="scripts"
## DEFINING GLOBAL VARS NAME
global_file_name="global_funcs.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../${scripts_dir}/${global_file_name}"


#######################
### INPUT VARIABLES ###
#######################

## DEFINING VARIABLES
input_molecule_name="${1-LIG7}"

## DEFINING FORCE FIELD
forcefield="${2-charmm36-jul2020.ff}"

## DEFINING FOLDER WITH INPUTS
input_stretch_folder="${main_dir}/inputs_prep_stretch_ligand"

## DEFINING IF REWRITE IS DESIRED
rewrite=false
# true if you want rewrite

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING DEFAULT DIRECTORY
default_dir="stretch_lig"

## DEFINING GROMACS COMMAND
gromacs_command=gmx

######################
### DEFINING PATHS ###
######################

## DEFINING MDP INFO
mdp_file="stretch_lig.mdp"
path_input_mdp="${MDLIG_INPUT_FILES_MDP}/${mdp_file}"

## MD BUILDER CODE
py_get_ligand_indices="get_ligand_indices.py"
path_py_ligand_indices="${MDBUILDER}/applications/nanoparticle/get_ligand_indices.py"

## FORCEFIELD
path_forcefield_file="${MDLIG_FF_DIR}/${forcefield}"

## PATH TO MOLECULE
path_forcefield_molecule="${main_dir}/final_structures/${forcefield}"
path_molecule="${path_forcefield_molecule}/${input_molecule_name}"

## DEFINING DIRECTORY
path_molecule_stretch="${path_molecule}/${default_dir}"

## DEFINING PATHS
path_gro="${path_molecule}/${input_molecule_name}.gro"
path_itp="${path_molecule}/${input_molecule_name}.itp"
path_prm="${path_molecule}/${input_molecule_name}.prm"
path_top="${path_molecule}/em_setup_files/em.top"

## CHECKING IF EXISTS
stop_if_does_not_exist "${path_molecule}"

## DEFINING OUTPUT INDEX
output_ndx="index.ndx"
path_output_ndx="${path_molecule_stretch}/${output_ndx}"

#####################
### MDP FILE INFO ###
#####################

## DEFINING DEFAULT GROUPS
default_group_1="BOUND_SULFURS"
default_group_2="END_GROUP_ATOM"

## DEFINING TIME STEPS
nsteps="5000" # 2 ps
dt="0.002" # rapid time
spring_constant="-1000" # kJ/mol/nm2
pull_rate="0" # nm/ps

## DEFINING DEFAULT BOX SIZE
dist_to_edge="5"
output_dist_to_edge="0.01"

## DEFINING MAXWARN
maxwarn=5

## DEFINING NUMBER OF CORES
num_cores="10"

#################
### MAIN CODE ###
#################

## CHECKING IF STRETCHING CODE ALREDY EXISTS
if [[ ! -e "${path_molecule_stretch}" ]] || [[ "${rewrite}" == true ]]; then
	## CREATING DIRECTORY
	create_dir "${path_molecule_stretch}" -f

	## GOING TO DIRECTORY
	cd "${path_molecule_stretch}"

	## GETTING INDEX FILE
	${gromacs_command} make_ndx -f "${path_gro}" -o "${path_output_ndx}" << INPUTS
q
INPUTS

	## RUNNING PYTHON SCRIPT TO GET INDICES
	python "${path_py_ligand_indices}" --path_gro "${path_gro}" \
									   --path_itp "${path_itp}" \
									   --path_ndx "${path_output_ndx}"

	## GETTING MDP FILE FOR STRETCHING
	echo "Copying over mdp file!"
	path_output_mdp="${path_molecule_stretch}/${mdp_file}"
	cp "${path_input_mdp}" "${path_output_mdp}"

	## EDITING MDP FILE
	sed -i "s/_DT_/${dt}/g" "${path_output_mdp}"
	sed -i "s/_NSTEPS_/${nsteps}/g" "${path_output_mdp}"
	sed -i "s/_GROUP1_/${default_group_1}/g" "${path_output_mdp}"
	sed -i "s/_GROUP2_/${default_group_2}/g" "${path_output_mdp}"
	sed -i "s/_PULLRATE_/${pull_rate}/g" "${path_output_mdp}"
	sed -i "s/_SPRING_/${spring_constant}/g" "${path_output_mdp}"

	## EDITING INPUT GRO FILE TO BE LARGE IN TERMS OF BOX SIZE
	${gromacs_command} editconf -f "${path_gro}" \
								-o "${path_molecule_stretch}/initial.gro" \
								-d "${dist_to_edge}"

	## GETTING BOX SIZE
	read -a box_size <<< $(gro_measure_box_size ${path_gro})

	## GETTING DISTANCE
	half_box=$(awk -v size=${box_size[0]} 'BEGIN{ printf "%.5f",size/2.0}') # nms

	## EDITING MDP
	sed -i "s/_PULLINIT_/${half_box}/g" "${path_output_mdp}"

	## PERFORMING GROMPP
	${gromacs_command} grompp -f "${path_output_mdp}" \
							  -c "${path_molecule_stretch}/initial.gro" \
							  -p "${path_top}" \
							  -n "${path_output_ndx}" \
							  -o "stretch.tpr" \
							  -maxwarn "${maxwarn}"
	## RUNNING GROMACS
	${gromacs_command} mdrun -nt "${num_cores}" -deffnm "stretch"

	## CHECKING IF GRO FILE IS OUTPUTTED
	if [ ! -e "stretch.gro" ]; then
		echo "Pulling did not complete!"
		echo "Stopping here to prevent subsequent errors"
		exit
	else
		echo "Pulling is completed!"
		## UPON COMPLETION, REPLACE OUTPUT GRO FILE WITH STRETCH FILE
		cp "${path_gro}" "${path_molecule}/${input_molecule_name}_old.gro"

		## REMOVING PATH GRO
		if [ -e "${path_molecule}/${input_molecule_name}_old.gro" ]; then
			rm "${path_gro}"
		fi

		## EDITING INPUT GRO FILE TO BE LARGE IN TERMS OF BOX SIZE
		${gromacs_command} editconf -f "${path_molecule_stretch}/stretch.gro" \
									-o "${path_gro}" \
									-d "${output_dist_to_edge}"

	fi

else
	echo "${path_molecule_stretch} exists, skipping!"
fi


