# MDLigands

Welcome to MDLigands! The purpose of this project is to generate ligands necessary for molecular dynamics (MD) simulations. The ligands will then be transferrable to any MD simulation for any force field. For more details, please check out the wikipedia page for this: https://gitlab.com/alexkchew/MDLigands/wikis/home

# Installation instructions

To clone this git repository:

`git clone https://gitlab.com/alexkchew/MDLigands.git ./`

# Simplified instruction: generating new ligands

1. Draw the ligand in Avogadro
2. Store the ligand in a *.mol2 file
3. Go to the CGenFF website to generate *.str file: https://cgenff.umaryland.edu/
4. Check out Google excel that has all the ligands: https://docs.google.com/spreadsheets/d/1mCaWR9GhFxxAYXbZ7v6mZLo5cXeYOLqEoRUlSIBitxM/edit?usp=sharing. Add your ligand with its residue name. If you need access to the excel sheet, please contact Alex. Ideally, you should not have matching residue names with another ligand (thus, enabling us to do mixed-ligands). Please make sure that there is no clashes with your input name and previous ligands.
5. Copy the *.mol2 and *.str file into the `raw_files` folder within MDLigands
6. Run `prep_single_ligand.sh` for a single ligand or `prep_multi_ligands.sh` for multiple ligands.
---
Example for **single** ligand:
```
bash prep_single_ligand.sh dodecanethiol DEC charmm36-jul2017.ff cgenff_charmm2gmx_py2.py
```
Here, `dodecanethiol` is the molecule name, `DEC` is the residue name, `charmm36-jul2017.ff` is the force field, and `cgenff_charmm2gmx_py2.py` is the CGenFF python code.

---

Example for **multiple** ligand
```
bash prep_multi_ligands.sh
```
Note that for multiple ligand, you will need to change the `prep_multi_ligands.sh` script, which has an array for the molecule name and residue name.

---

Additional details can be found in the wikipedia.

# Simplified instruction: Using the ligands for MD simulations

Now that you have generated the ligands, you should have the following in `MDLigands > final_ligands > forcefield > ligand_name` (for example, dodecanethiol):
```
dodecanethiol.gro
dodecanethiol.itp
dodecanethiol.prm
em_setup_files
prep_files
```
You can use the gro, itp, and parameter file for subsequent simulations. `em_setup_files` and `prep_files` are folders containing information about the energy minimization and output of CGenFF. Note that ligands with sulfur head groups are combined with its hydrogen attachment (since we assume it gets lost when adsorbing onto a metal surface).

---

# Simplified instruction: Generating solvents for MD simulations
You could also use the same code to generate solvents by energy minimization. Repeat "generate new ligand" instructions but save the files in `MDLigands > solvents > raw_files`. Then, name the solvent and the residue name (see google sheet):
https://docs.google.com/spreadsheets/d/1mCaWR9GhFxxAYXbZ7v6mZLo5cXeYOLqEoRUlSIBitxM/edit#gid=294113188

You can run the following command to prepare a single solvent
```
bash prep_single_solvent.sh methanol MET charmm36-jul2017.ff
```
Change `methanol` and `MET` with solvents that you care about. Now, you should have all final structures within `MDLignads > solvents > charmm36-jul2017.ff > methanol`. You should have the following files
```
em_setup_files
methanol.gro
methanol.itp
methanol.prm
prep_files
```

Alternatively, you could run multiple solvents by modifying `prep_multi_solvent.sh` and running:
```
bash prep_multi_solvent.sh
```
---

# Simplified instruction: Converting ChemDraw structures to .mol2 files
Note that this needs Openbabel! If you do not have it, please check out the wiki:
https://gitlab.com/alexkchew/MDLigands/-/wikis/Tutorials/0b_Make_ligands_with_ChemDraw

To convert `*.cdx` files to `*.mol2`, save all the `.cdx` files in `MDLigands/raw_files`. Then, perform the following:
```
bash openbabel_convert_cdx_to_mol2.sh
```

This script will go through and look for all `.cdx` files within `MDLigands/raw_files` and convert them into `.mol2` files. Then, you could use the `.mol2` files for generating force field parameters, simulations, etc. If you need the script to run in a different folder, add a path input, such as:
```
bash openbabel_convert_cdx_to_mol2.sh PATH
```

Note, I did find that when using cdx to mol2 for long Rotello ligands, the conformation is not what I was expecting (a linear chain). You may need to revert back to modifying through Avogadro if you want a specific conformation. In terms of MD simulations, the initial structure should not matter significantly, other than the generation of initial systems (i.e. if a ligand is twisted, it could overlap with other ligands, resulting in LINCS warnings). 


