#!/bin/bash

# prep_single_peptide.sh
# This script runs through energy minimization of a single peptide system. 
# This is intended to energy minimize the peptide in preparation for subsequent simulations. 

# Originally written by: Alex K. Chew (alexkchew@gmail.com)
# Created on: 05/30/2019
# Adapted for peptides on: 08/15/2019

## USAGE EXAMPLE:
# bash prep_single_peptide.sh hexylamine HMN charmm36-jul2017.ff

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING SCRIPTS DIRECTORY
scripts_dir="scripts"
## DEFINING GLOBAL VARS NAME
global_file_name="global_funcs.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/${scripts_dir}/${global_file_name}"

################################
### DEFINING INPUT VARIABLES ###
################################

## DEFINING VARIABLES
input_molecule_name="$1"
input_residue_name="$2"
## DEFINING FORCE FIELD
forcefield="${3:-charmm36-jul2017.ff}"
## DEFINING PYTHON SCRIPT
python_script="${4:-cgenff_charmm2gmx_py2.py}"

####################
### BASH SCRIPTS ###
####################
## CONVERSION USING CGENFF
bs_prep_lig_to_cgenff="${MDLIG_SCRIPT_DIR}/prep_ligands_charmm36.sh"
## DEFINING BASH SCRIPTS
bs_prep_ligand_em="${MDLIG_SCRIPT_DIR}/prep_ligand_energy_min.sh"

######################
### DEFINING PATHS ###
######################
### MISCELLANEOUS DIRECTORIES
prep_file="prep_files"
em_setup_files="em_setup_files"

## DEFINING RAW DIRECTORY
path_raw_dir="${MDLIG_PEPTIDES_RAW}"
path_final_dir="${MDLIG_PEPTIDES_FINAL}"

### DEFINING INPUT GRO AND ITP FILE
input_gro_file="${input_molecule_name}_em.gro"
input_itp_file="${input_molecule_name}.itp"
input_prm_file="${input_molecule_name}.prm"

## DEFINING MOLECULE PATH
path_forcefield_molecule="${path_final_dir}/${forcefield}"
path_molecule="${path_forcefield_molecule}/${input_molecule_name}"
## DEFINING PREP FILE
path_molecule_prep="${path_molecule}/${prep_file}"
## PATH TO SETUP FILE
path_em_setup_files="${path_molecule}/${em_setup_files}"

## DEFINING FILE PATH
path_molecule_em_itp="${path_molecule_prep}/${input_itp_file}" # itp file
path_molecule_em_gro="${path_em_setup_files}/${input_gro_file}"
path_molecule_em_prm="${path_molecule_prep}/${input_prm_file}"

###############
### OUTPUTS ###
###############

path_output_gro="${path_molecule}/${input_molecule_name}.gro"
path_output_itp="${path_molecule}/${input_molecule_name}.itp"
path_output_prm="${path_molecule}/${input_molecule_name}.prm"

###################
### MAIN SCRIPT ###
###################

##############
### CGENFF ###
##############
echo ""
echo "--> Running command: bash "${bs_prep_lig_to_cgenff}" "${input_molecule_name}" "${input_residue_name}" "${forcefield}" "${python_script}" "${path_raw_dir}" "${path_final_dir}""
## USING CGENFF FOR THE SPECIFIC LIGAND
bash "${bs_prep_lig_to_cgenff}" "${input_molecule_name}" "${input_residue_name}" "${forcefield}" "${python_script}" "${path_raw_dir}" "${path_final_dir}"

###########################
### ENERGY MINIMIZATION ###
###########################

### RUNNING ENERGY MINIMIZATION BASH SCRIPT
bash "${bs_prep_ligand_em}" "${input_molecule_name}" "${input_residue_name}" "${forcefield}" "${path_final_dir}"

### CHECKING IF YOU GET FILES CORRECT
stop_if_does_not_exist "${path_molecule_em_itp}"
stop_if_does_not_exist "${path_molecule_em_gro}"
stop_if_does_not_exist "${path_molecule_em_prm}"

### COPYING OVER WITH EDITCONF TO MAKE BOX AS SMALL AS POSSIBLE
gmx editconf -f "${path_molecule_em_gro}" -o "${path_output_gro}" -d 0
cp "${path_molecule_em_itp}" "${path_output_itp}"
cp "${path_molecule_em_prm}" "${path_output_prm}"

## PRINTING SUMMARY
echo -e "\n********** SUMMARY **********"
echo "CREATED peptideS FOR: ${input_molecule_name}"
echo "RESIDUE NAME: ${input_molecule_resname}"
echo "FORCE FIELD: ${forcefield}"
echo "FULL PATH TO peptideS: ${path_final_dir}"
echo "FULL PATH TO YOUR MOLECULE: ${path_molecule}"
echo "Good luck in the simulations!"

