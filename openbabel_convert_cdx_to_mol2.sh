#!/bin/bash

# openbabel_convert_cdx_to_mol2.sh
# This function converts chemdraw structures to mol2 files. 
# It will search for all possible *.cdx file, then run open babel to convert them to mol2
# It will also run energy minimization
#
# Written by: Alex K. Chew (04/16/2020)

# INPUT VARIABLES
# 	$1: input raw directory folder

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING SCRIPTS DIRECTORY
scripts_dir="scripts"
## DEFINING GLOBAL VARS NAME
global_file_name="global_funcs.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/${scripts_dir}/${global_file_name}"

## DEFINING MAIN DIRECTORY TO WORK ON
current_raw_dir="${1-${MDLIG_RAW_DIR}}"

#######################################
### DEFAULT VARIABLES FOR OPENBABEL ###
#######################################

## FORCE FIELD
em_forcefield="MMFF94s"
## TOLERENCE
tolerence="1e-6"
## DEFINING MAX NUMBER OF STEPS
max_steps="50000"

## FINDING ALL CDX FILES
read -a cdx_files <<< $(ls ${current_raw_dir}/*.cdx )

## CREATING TRASH
path_trash="${current_raw_dir}/openbabel_initial_structures"

## PRINTING
echo "---- CDX FILES ----"
for each_cdx in ${cdx_files[@]}; do

	echo "=== ${each_cdx} ==="

	## GETTING BASENAME
	cdx_basename=$(basename ${each_cdx})

	## GETTING THE FILE PREFIX
	file_prefix="${cdx_basename%.cdx}"

	## FINDING MOL2
	mol2_file="${file_prefix}.mol2"
	path_mol2="${current_raw_dir}/${mol2_file}"

	## CHECKING IF EXISTING
	if [ ! -e "${path_mol2}" ]; then
		## CREATING TRASH DIRECTORY
		if [ ! -e "${path_trash}" ]; then
			mkdir -p "${path_trash}"
		fi

		echo "Performing mol2 conversion with openbabel"

		## DEFINING TEMP FILE
		temp_mol2_file="${file_prefix}_initial.mol2"

		## CONVERTING CDX TO MOL2 AND ADD HYDROGENS
		obabel -icdx "${each_cdx}" -O "${path_trash}/${temp_mol2_file}" -h

		## PERFORMING ENERGY MINIMIZATION
		obminimize -sd -ff "${em_forcefield}" \
					   -c "${tolerence}" \
					   -n "${max_steps}" \
					   -o "mol2" \
					   "${path_trash}/${temp_mol2_file}" > "${path_mol2}"
		## PRINTING
		echo "Openbabel conversion is complete!"

	else
		echo "${path_mol2} exists!"
	fi

done

