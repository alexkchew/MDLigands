#!/bin/bash

# prep_multi_ligands.sh
# This script prepares multiple ligands. We will use the preparation single ligand code and expand on it.
# Originally written by: Alex K. Chew (alexkchew@gmail.com)
# Created on: 05/09/2091
## USAGE:
#   bash prep_multi_ligands.sh

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING SCRIPTS DIRECTORY
scripts_dir="scripts"
## DEFINING GLOBAL VARS NAME
global_file_name="global_funcs.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/${scripts_dir}/${global_file_name}"

#############################
### DEFINING INPUT SCRIPT ###
#############################

## SINGLE LIGAND SCRIPT
prep_single_lig_script="${MDLIG_MAIN_DIR}/prep_single_ligand.sh"

#############################
### PRE-DEFINED VARIABLES ###
#############################

## DEFINING FORCE FIELD
forcefield="charmm36-jul2017.ff"

## DEFINING CGENFF CODE
cgenff_script="cgenff_charmm2gmx_py2.py"

#######################
### CREATING ARRAYS ###
#######################
## DEFINING MOLECULE NAME
declare -a ligand_molecule_name=("C3OH")
# "C11branch6NH2" "C11branch6CH3" "C11branch6COOH" "C11branch6CONH2" "C11branch6CF3"
# "C11double67NH2" "C11double67COOH" "C11double67CONH2"
# "C11double67CF3"
# "C11branch6OH"
# "C11CCH33" "C11NCH33"
# "ROT015" "ROT016"
# "ROTPE1" "ROTPE2" "ROTPE3" "ROTPE4"
# "C11COO" "C11NH3"
# "C11CF3" "C12CN" "C12CONH2" "C12OCH3" "C13NH2" "C13NHCNHNH2" "C13OH"
# "C11OH" "C11NH2" "C11COOH" "C11CONH2"
# "ROT001" "ROT002" "ROT003" "ROT004"
# "ROT005" "ROT006" "ROT007" "ROT008"

## DEFINING RESIDUE NAME (MUST MATCH MOLECULE NAME!)
declare -a ligand_residue_name=("C3O")
# "BNH" "BDO" "BCO" "BCN" "BCF"
# "DBN" "DBC" "DBB"
# "DBF"
# "DBO"
# "CCB" "CNB"
# "R15" "R16"
# "PE1" "PE2" "PE3" "PE4"
# "COM" "CNP"
# "UF3" "DCN" "DAD" "DMX" "TAM" "TGD" "TOH"
# "COH" "NH2" "COO" "CON"
# "RO1" "RO2" "RO3" "RO4"
# "RO5" "RO6" "RO7" "RO8"

###################
### MAIN SCRIPT ###
###################

## FINDING TOTAL MOLECULES
total_molecules=$((${#ligand_molecule_name[@]}-1))

## LOOPING THROUGH EACH MOLECULE NAME
for index in $(seq 0 ${total_molecules}); do
    ## DEFINING MOLECULE NAME AND RESIDUE NAME
    molecule_name="${ligand_molecule_name[index]}"
    molecule_resname="${ligand_residue_name[index]}"
    ## RUNNING PREPARATION CODE
    bash "${prep_single_lig_script}" "${molecule_name}" "${molecule_resname}" "${forcefield}" "${cgenff_script}"
done



